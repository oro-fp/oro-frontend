import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Scheduele from "@/views/Scheduele.vue";
import Dashboard from "@/views/Dashboard.vue"
import EditClient from "@/views/EditClient.vue"
import Activities from "@/views/Activities.vue"

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Scheduele',
    component: Scheduele
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/editclient',
    name: 'EditClient',
    component: EditClient
  },
  {
    path: '/activities',
    name: 'Activities',
    component: Activities
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
