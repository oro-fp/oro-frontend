import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

Vue.config.productionTip = false

import ApolloClient from 'apollo-boost'
import VueApollo from 'vue-apollo'

const apolloClient = new ApolloClient({
    // You should use an absolute URL here
    uri: 'http://localhost:4000'
})

Vue.use(VueApollo)
const apolloProvider = new VueApollo({
    defaultClient: apolloClient,
})


new Vue({
    router: router,
    store: store,
    apolloProvider: apolloProvider,
    render: h => h(App),
}).$mount('#app')
