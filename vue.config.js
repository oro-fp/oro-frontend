// vue.config.js

/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */
module.exports = {
    filenameHashing: false,
    publicPath: "",
    css: {
        loaderOptions: {
            sass: {
                prependData: `
                      @import "@/assets/css/global.scss";
                      `
            }
        }
    }
}